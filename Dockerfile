FROM php:8.1-cli

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/

RUN apt update && apt install -y libicu-dev
RUN install-php-extensions @composer intl xdebug
